import React from 'react';
import {Switch} from 'react-router-dom';
import Route from './Route';

import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';

import Dashboard from '../pages/Dashboard';
import Profile from '../pages/Profile';
import NewTextPost from '../pages/NewTextPost';
import MyTextPost from '../pages/MyTextPost';
import TextPostPage from '../pages/TextPostPage';
import SearchTextPost from '../pages/SearchTextPost';

export default function Routes(){
    return(
        <Switch>
            <Route path="/" exact component={SignIn}/>
            <Route path="/register" component={SignUp}/>

            <Route path="/dashboard" component={Dashboard} isPrivate={true}/>
            <Route path="/newtextpost" component={NewTextPost} isPrivate={true}/>
            <Route path="/mytextpost" component={MyTextPost} isPrivate={true}/>
            <Route path="/textpost/content/:postId" component={TextPostPage} isPrivate={true}/>
            <Route path="/profile" component={Profile} isPrivate={true}/>
            <Route path="/search" component={SearchTextPost} isPrivate={true}/>
            <Route path="/edittextpost/:postId" component={NewTextPost} isPrivate={true}/>
        </Switch>
    );

}