import styled from 'styled-components';

export const SearchContent = styled.div`
    display: block;
    max-width: 400px;
    width: 100%;


    form{
        display: flex;
        flex-direction: row;

        input{
            background: #FFF;
            border: 1px solid #ccc;
            border-right: 0;
            border-radius: 4px 0 0 4px;
            height: 36px;
            color: #000;
            padding: 0 15px;
            width: 100%;
        }

        button{
            background-color: #FFF;
            border: 1px solid #ccc;
            border-left: none;
            border-radius: 0 4px 4px 0;
            cursor: pointer;
            font-size: 16px;
            height: 36px;
            padding: 0 10px;
        }
    }
`