import React from 'react';
import { Form, Input } from '@rocketseat/unform';
import { MdSearch } from 'react-icons/md';
import { SearchContent } from './styles';
import { useLocation } from 'react-router-dom';

export default function Search() {
    const searchValue = new URLSearchParams(useLocation().search).get('text');

    function HandleSubmit(data){
        window.location.href = `/search?text=${data.search}`;
    }

    return (
        <SearchContent>
            <Form initialData={{search: searchValue}} onSubmit={HandleSubmit}>
                <Input name="search" placeholder="Procurar por..."/>
                <button type="submit"><MdSearch color="#BD5E2D" /></button>
            </Form>
        </SearchContent>
    );
}