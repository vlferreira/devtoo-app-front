import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { MdHome, MdList } from 'react-icons/md';
import Search from '~/components/Search';
import logo from '~/assets/logo2.svg';
import { Container, Content, Profile, Icons } from './styles';

export default function Header() {
    const profile = useSelector(state => state.user.profile);

    return (
        <Container>
            <Content>
                <nav>
                    <img src={logo} alt="DevToo" />
                    <Link to="/dashboard" alt="Página Inicial" title="Página Inicial" ><MdHome color="#BD5E2D" size={20} /></Link>
                </nav>
                <Search />
                <aside>
                    <Icons>
                        <Link to="/mytextpost" alt="Minhas Postagens" title="Minhas Postagens"><MdList color="#BD5E2D" size={20} /></Link>
                    </Icons>
                    <Profile>
                        <div>
                            <strong>{profile.name}</strong>
                            <Link to="/profile">Meu Perfil</Link>
                        </div>
                        <img src={profile.avatar ? profile.avatar.url : 'https://avatars.dicebear.com/api/bottts/lovely-stan.svg?options[colors][]=orange&options[primaryColorLevel]=600&options[secondaryColorLevel]=400&options[width]=50&options[height]=50&options[background]=%23FF00F7'} alt={profile.name}/>
                    </Profile>
                </aside>
            </Content>
        </Container>
    );
}