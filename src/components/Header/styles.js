import styled from 'styled-components';

export const Container = styled.div`
    background: #FFF;
    padding: 0 30px;
`;

export const Content = styled.div`
    align-items: center;
    display: flex;
    height: 64px;
    justify-content: space-between;
    margin: 0 auto;
    max-width: 1100px;

    nav{
        align-items: center;
        display: flex;

        img{
            margin-right: 20px;
            padding-right: 20px;
            border-right: 1px solid #eee;
            width: 50px;
        }

        a{
            color: #7159c1;
            font-weight: bold;
        }
    }

    aside{
        align-items: center;
        display: flex;
    }
`;

export const Profile = styled.div`
    border-left: 1px solid #eee;
    display: flex;
    margin-left: 20px;
    padding-left: 20px;

    div{
        margin-right: 10px;
        text-align: right;

        strong{
            display: block;
            color: #333;
        }

        a{
            color: #999;
            display: block;
            font-size: 12px;
            margin-top: 2px;
        }

    }
    img{
        border-radius: 50%;
        height: 36px;
        width: 36px;
    }
`;

export const Icons = styled.div`
    margin-right: 15px;
`;