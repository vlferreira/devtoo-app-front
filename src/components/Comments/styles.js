import styled from 'styled-components';
import { darken } from 'polished';

export const CommentsList = styled.div`
    display: block;
    background-color: #FFF;
    border-radius: 0 0 5px 5px;
    padding: 30px 15px 20px;

    > h2{
        border-top: 1px solid #CCC;
        padding-top: 20px;
    }

    form{
        display: flex;
        flex-direction: column;
        margin-top: 30px;

        > h2{
            font-size: 15px;
            margin-bottom: 10px;
        }

        textarea{
            background: #FFF;
            border: 1px solid #ccc;
            border-radius: 5px;
            min-height: 80px;
            color: #000;
            padding: 15px 15px;
            width: 100%;
        }

        > button {
            background: #41a219;
            border: 0;
            border-radius: 4px;
            color: #FFF;
            cursor: pointer;
            font-size: 16px;
            font-weight: bold;
            height: 36px;
            margin: 20px 0 0;
            max-width: 200px;
            transition: background .2s;

            &:hover{
                background: ${darken(.04, '#41a219')};
            }
        }
    }
`;

export const CommentsContent = styled.div`
    align-items: center;
    display: flex;
    margin-top: 10px;
    padding-bottom: 10px;
    
    &:not(:last-of-type){
        border-bottom: 1px solid #CCC;
    }

    .sign{
        font-weight: bold;
        font-style: italic;
        margin-bottom: 15px;
    }
`

export const CommentWarning = styled.div`
    background-color: #FFF;
    border-radius: 5px;
    display: block;
    text-align: center;
    padding: 5px;
`;

export const Comment= styled.div`
    display: block;
`;

export const Votes= styled.div`
    display: block;
    margin-right: 20px;
    text-align: center;
    
    > .icon{
        cursor: pointer;
        font-size: 20px;
    }
`;
export const Avatar= styled.div`
    display: block;
    margin-right: 20px;

    img{
        border-radius: 50%;
        height: 36px;
        width: 36px;
    }
`