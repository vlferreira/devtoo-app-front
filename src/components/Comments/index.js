import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Input } from '@rocketseat/unform';
import * as Yup from 'yup';
import api from '~/services/api';
import { parseISO, format } from 'date-fns';
import { MdKeyboardArrowUp, MdKeyboardArrowDown } from 'react-icons/md';
import { CommentsList, CommentsContent, CommentWarning, Votes, Comment, Avatar } from './styles';
import { insertCommentRequest, updateCommentRequest } from '~/store/modules/comments/actions';

const schema = Yup.object().shape({
    text_content: Yup.string().required('O texto do comentário é obrigatório')
});

export default function Comments(props) {
    const userId = useSelector(state => state.user.profile).id;
    const postId = props.postId;
    const [comments, setComments] = useState([]);
    const dispatch = useDispatch();
    
    useEffect(() => {
        async function LoadComments(){
            const response = await api.get(`comments/${postId}`);

            
            const data = response.data.comments.map(comments => ({
                ... comments
            }));

            console.log(data);

            setComments(data);
        }

        LoadComments();
    }, []);

    function handleSubmit(data){
        data.reference_post_id = postId;
        data.user_id = userId;
        dispatch(insertCommentRequest(data));
    }

    function HandleVote(vote, votes, comment_id){
        const data = {
            votes: vote == 1 ? votes + 1 : votes - 1,
            id: comment_id
        }

        dispatch(updateCommentRequest(data));
    }

    return (
        <CommentsList>
            <h2>Comentários</h2>

            { comments.length > 0 ?
                comments.map(comment => (
                    <CommentsContent key={comment.id}>
                        <Votes>
                            <MdKeyboardArrowUp className="icon" onClick={() => HandleVote(1, comment.votes, comment.id)}/>
                            <div>{comment.votes}</div>
                            <MdKeyboardArrowDown className="icon" onClick={() => HandleVote(-1, comment.votes, comment.id)}/>
                        </Votes>
                        <Avatar>
                            <img src={comment.avatar ? comment.avatar.url : 'https://avatars.dicebear.com/api/bottts/lovely-stan.svg?options[colors][]=orange&options[primaryColorLevel]=600&options[secondaryColorLevel]=400&options[width]=50&options[height]=50&options[background]=%23FF00F7'} alt={comment.author.name}/>
                        </Avatar>
                        <Comment>
                            <div className="sign">Por {comment.author.name}, em { format(parseISO(comment.created_at), 'dd/MM/yyyy')}</div>
                            <div>{comment.text_content}</div>
                        </Comment>
                    </CommentsContent>
                )) 
            :
                <CommentWarning>
                    <p>Nenhum comentário encontrado. Seja o primeiro!</p>
                </CommentWarning>
            }

            <Form onSubmit={handleSubmit} schema={schema}>
                <h2>Novo comentário</h2>
                <Input name="text_content" multiline placeholder="Digite o seu comentário..."/>
                <button type="submit">Enviar</button>
            </Form>
        </CommentsList>
    )
}