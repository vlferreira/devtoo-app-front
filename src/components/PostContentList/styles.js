import styled from 'styled-components';
import { darken } from 'polished';

export const PostsList = styled.div`
    display: block;
`;

export const PostContent = styled.div`
    background-color: #FFF;
    border-radius: 5px;
    display: block;
    height: 65px;
    max-height: 65px;
    margin-bottom: 10px;
    text-align: left;
    overflow: hidden;

    > a {
        color: #000;
        display: block;
        padding: 10px;
        text-decoration: none;
    }

    h2 {
        font-size: 16px;
        margin-bottom: 5px;

        span{
            font-size: 12px;
            font-weight: normal;
        }

        .label-quest, .label-info{
            border-radius: 4px;
            color: #FFF;
            display: inline-block;
            margin-left: 20px;
            padding: 1px 5px;
            vertical-align: text-top;
        }

        .label-quest{
            background-color: #6540bb;
        }

        .label-info{
            background-color: #ba5d32;
        }
    }

    .author{
        font-size: 12px;
        font-style: italic;
        margin-top: 10px;
    }
`;

export const PostWarning = styled.div`
    background-color: #FFF;
    border-radius: 5px;
    display: block;
    text-align: center;
    padding: 5px;
`;