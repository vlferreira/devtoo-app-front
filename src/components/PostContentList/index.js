import React, { useState, useEffect } from 'react';
import { PostsList, PostContent, PostWarning} from './styles';
import api from '~/services/api';
import { parseISO, format } from 'date-fns';
import { Link } from 'react-router-dom';
import parse from 'html-react-parser';


export default function PostContentList(props) {
    const [textpost, setTextPost] = useState([]);

    const userId = props.userId;
    const searchParams = props.searchParams;

    useEffect(() => {
        async function loadTextPost(){
            let response;

            if(userId && userId != 0){
                response = await api.get('textpost', {
                    params: { userId }
                });
            } else {
                if(searchParams){
                    response = await api.get('textpost/search', {
                        params: { 
                            textSearch: searchParams.get('text') != null ? searchParams.get('text') : "",
                            textType: searchParams.get('textType') != null ? searchParams.get('textType') : 1,
                            order: searchParams.get('orderBy') != null ? searchParams.get('orderBy') : 1
                        }
                    });
                } else {
                    response = await api.get('textpost');
                }
            }
            
            const data = response.data.map(textpost => ({
                ... textpost
            }));

            setTextPost(data);
        }

        loadTextPost();
    }, [userId]);
    
    return (
        <PostsList>
            { textpost.length > 0 ?
                textpost.map(tp => (
                    <PostContent key={tp.id}>
                        <Link to={`/textpost/content/${tp.id}`}>
                            <div>
                                <h2>{tp.title}<span className={tp.text_type == 1 ? "label-quest" : "label-info" }>{tp.text_type == 1 ? "Dúvida" : "Informativo"}</span></h2>
                            </div>
                            <div className="author">Em { format(parseISO(tp.created_at), 'dd/MM/yyyy') }, por {tp.author.name}</div>
                        </Link>
                    </PostContent>
                )) 
            :
                <PostWarning>
                    <p>Nenhuma postagem encontrada</p>
                </PostWarning>
            }
        </PostsList>
    );
}