import styled from 'styled-components';
import { darken } from 'polished';


export const Container = styled.div`
    margin: 50px auto;
    max-width: 1000px;

    
    form{
        display: flex;
        flex-direction: column;
        margin-top: 30px;

        input{
            background: #FFF;
            border: 0;
            border-radius: 4px;
            height: 44px;
            padding: 0 15px;
            color: #000;
            margin: 0 0 10px;
        }

        select{
            background: #FF;
            border: 0;
            border-radius: 4px;
            height: 40px;
            padding: 0 13px;
            margin: 0 0 10px;
        }

        > button {
            background: #41a219;
            border: 0;
            border-radius: 4px;
            color: #FFF;
            cursor: pointer;
            font-size: 16px;
            font-weight: bold;
            height: 44px;
            margin: 20px 0 0;
            transition: background .2s;

            &:hover{
                background: ${darken(.04, '#41a219')};
            }
        }

        hr {
            background color: rgba(255, 255, 255, .2);
            border: 0;
            height: 1px;
            margin: 10px 0 20px;
        }

        a{
            color: #FFF;
            font-size: 16px;
            margin-top: 15px;
            opacity: .8;

            &:hover{
                opacity: 1;
            }
        }

        span{
            align-self: flex-start;
            color: #FB6F91;
            font-weight: bold;
            margin: 0 0 10px;
        }
        .sun-editor .se-wrapper .se-wrapper-code{
            height: 400px !important;
        }
    }
`