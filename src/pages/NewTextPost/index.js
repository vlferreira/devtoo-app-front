import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import SunEditor from 'suneditor-react';
import api from '~/services/api';
import { Form, Input, Select } from '@rocketseat/unform';
import { updateTextPostRequest, insertTextPostRequest } from '~/store/modules/textpost/actions';
import * as Yup from 'yup';
import history from '~/services/history';
import { Container } from './styles';
import 'suneditor/dist/css/suneditor.min.css';

const schema = Yup.object().shape({
    text_type: Yup.number().moreThan(0, 'O tipo da postagem é obrigatório').required('O tipo da postagem é obrigatório'),
    title: Yup.string().required('O título é obrigatório')
});


export default function NewTextPost() {
    const userId = useSelector(state => state.user.profile).id;
    const { postId } = useParams();
    const dispatch = useDispatch();

    const [textPost, setTextPost] = useState();
    const [textContent, setTextContent] = useState();
    const [textType, setTextType] = useState();
    
    useEffect(() => {
        async function loadTextPost(){
            if(postId){
                const response = await api.get(`textpost/${postId}`); 
                const data = response.data;
                
                if(data && userId == data.user_id){
                    setTextContent(data.text_content);
                    setTextType(data.text_type);
                    setTextPost(data);
                } else {
                    history.push("/newtextpost");
                }
            }
        }

        loadTextPost();
    }, [postId]);

    const optionsType = [
        {id: '1', title: 'Dúvidas'},
        {id: '2', title: 'Tutoriais e Informativos'}
    ];
    
    function handleSubmit(data){
        data.id = postId;
        data.text_content = textContent;
        data.user_id = userId;
        if(postId){
            dispatch(updateTextPostRequest(data));
        } else {
            dispatch(insertTextPostRequest(data));
        }
    }

    function HandleTextBoxChange(content){
        setTextContent(content);
    }

    return(
        <Container>
            <div className="page-title">{textPost ? 'Editar Postagem' : 'Nova Postagem'}</div>

            <Form initialData={textPost} onSubmit={handleSubmit} schema={schema}>
                <Select name="text_type" options={optionsType} defaultValue={0} placeholder="Tipo da Postagem:" value={textType} />
                <Input name="title" placeholder="Título" />
                <SunEditor name="text_content" lang="pt_br" setDefaultStyle="height: 400px; font-size: 14px;" placeholder="" onChange={HandleTextBoxChange} setContents={textContent} />
                <button type="submit">Salvar</button>
            </Form>
        </Container>
    );
}