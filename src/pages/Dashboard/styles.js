import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin: 40px auto;
    max-width: 1100px;
    padding: 0 15px;
`;