import React from 'react';
import PostContentList from '~/components/PostContentList';
import { Container } from './styles';

export default function Dashboard() {
    return (
        <Container>
            <PostContentList userId='0' />
        </Container>
    );
}