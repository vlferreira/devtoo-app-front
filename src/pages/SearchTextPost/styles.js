import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin: 40px auto;
    max-width: 1100px;
`

export const SearchFilters = styled.div`
    background-color: #FFF;
    border-radius: 5px;
    margin-bottom: 30px;
    padding: 10px;

    > h2 {
        font-size: 14px;
        margin-bottom: 10px;
    }

    form {
        align-items: center;
        display: flex;  
        
        > label {
            align-items: center;
            display: flex;
            margin-right: 30px;

            span {
                margin-right: 15px;
            }

            > .select-comp{
                width: 200px;
            }
        }

        select {
            background: #FF;
            border: 1px solid #CCC;
            border-radius: 4px;
            height: 32px;
            padding: 0 13px;
            margin: 0 20px 0 0;
        }

        > button {
            background: #41a219;
            border: 0;
            border-radius: 4px;
            color: #FFF;
            cursor: pointer;
            font-size: 16px;
            font-weight: bold;
            float: right;
            height: 32px;
            transition: background .2s;
            padding: 0 15px;

            &:hover{
                background: ${darken(.04, '#41a219')};
            }
        }
    }
`