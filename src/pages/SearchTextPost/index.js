import React, { useState } from 'react';
import PostContentList from '~/components/PostContentList';
import { Form } from '@rocketseat/unform';
import Select from 'react-select'
import { Container, SearchFilters } from './styles';
import { useLocation } from 'react-router-dom';

export default function SearchTextPost() {
    const searchValue = new URLSearchParams(useLocation().search);
    const [optionTypeValue, setOptionTypeValue] = useState();
    const [optionOrderValue, setOptionOrderValue] = useState();

    const optionsOrder = [
        {value: '1', label: 'Mais recentes'},
        {value: '2', label: 'Mais antigos'}
    ];

    const optionsType = [
        {value: '1', label: 'Todos'},
        {value: '2', label: 'Dúvidas'},
        {value: '3', label: 'Tutoriais e informativos'}
    ];

    function handleSubmit(data){
        let urlParams = "";

        if(searchValue.get('text')){
            urlParams += `/search?text=${searchValue.get('text')}`;
        }
        if(optionTypeValue){
            urlParams += `&textType=${optionTypeValue.value}`;
        }
        if(optionOrderValue){
            urlParams += `&orderBy=${optionOrderValue.value}`;
        }

        window.location.href = urlParams;
    }

    function handleChangeTextType(value){
        setOptionTypeValue(value);
    }

    function handleChangeOrder(value){
        setOptionOrderValue(value);
    }

    return(
        <Container>
            <SearchFilters>
                <h2>Filtrar por</h2>
                <Form onSubmit={handleSubmit}>
                    <label>
                        <span>Tipo de postagem: </span>
                        <Select name="textType" options={optionsType} defaultValue={optionsType[(searchValue.get('textType') != null ? searchValue.get('textType') : 1) - 1]} className="select-comp" onChange={handleChangeTextType}/>
                    </label>
                    <label>
                        <span>Ordenar por: </span>
                        <Select name="orderBy" options={optionsOrder} defaultValue={optionsOrder[(searchValue.get('orderBy') != null ? searchValue.get('orderBy') : 1) - 1]} className="select-comp" onChange={handleChangeOrder}/>
                    </label>
                    <button type="submit">Filtrar</button>
                </Form>
            </SearchFilters>
            <PostContentList searchParams={searchValue} />
        </Container>
    );
}