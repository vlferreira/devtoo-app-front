import styled from 'styled-components';

export const Container = styled.div`
    align-self: center;
    margin-bottom: 30px;

    label {
        cursor: pointer;
        display: block;
        position: relative;

        &:hover{
            &:before{
                content: "";
                background-color: rgba(0, 0, 0, .6);
                border-radius: 50%;
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
            }

            &:after{
                content: "Alterar Imagem";
                color: #FFF;
                display: block;
                text-align: center;
                top: calc(50% - 8px);
                left: 0;
                right: 0;
                position: absolute;
            }
        }
        
        img {
            background: #eee;
            border: 3px solid rgba(255, 255, 255, .3);
            border-radius: 50%;
            display: block;
            height: 120px;
            width: 120px;
        }

        input{
            display: none;
        }
    }
`;