import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
    margin: 50px auto;
    max-width: 600px;

    form{
        display: flex;
        flex-direction: column;
        margin-top: 30px;

        input{
            background: rgba(0, 0, 0, .1);
            border: 0;
            border-radius: 4px;
            height: 44px;
            padding: 0 15px;
            color: #FFF;
            margin: 0 0 10px;

            &::placeholder {
                color: rgba(255, 255, 255, 0.7);
            }
        }

        button {
            background: #41a219;
            border: 0;
            border-radius: 4px;
            color: #FFF;
            cursor: pointer;
            font-size: 16px;
            font-weight: bold;
            height: 44px;
            margin: 5px 0 0;
            transition: background .2s;

            &:hover{
                background: ${darken(.03, '#41a219')};
            }
        }

        hr {
            background color: rgba(255, 255, 255, .2);
            border: 0;
            height: 1px;
            margin: 10px 0 20px;
        }

        a{
            color: #FFF;
            font-size: 16px;
            margin-top: 15px;
            opacity: .8;

            &:hover{
                opacity: 1;
            }
        }

        span{
            align-self: flex-start;
            color: #FB6F91;
            font-weight: bold;
            margin: 0 0 10px;
        }
    }

    > button {
        background: #F64C75;
        border: 0;
        border-radius: 4px;
        color: #FFF;
        cursor: pointer;
        font-size: 16px;
        font-weight: bold;
        height: 44px;
        margin: 10px 0 0;
        transition: background .2s;
        width: 100%;

        &:hover{
            background: ${darken(.08, '#F64C75')};
        }
    }
`