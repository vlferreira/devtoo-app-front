import styled from 'styled-components';

export const Wrapper = styled.div`
    height: 100%;
    background: linear-gradient(135deg, #5e3dc5, #BD5E2D);

    .page-title{
        color: #FFF;
        font-size: 20px;
        font-weight: bold;
        text-align: center;
    }

`;