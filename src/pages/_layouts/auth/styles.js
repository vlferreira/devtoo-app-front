import styled from 'styled-components';
import { darken } from 'polished';

export const Wrapper = styled.div`
    height: 100%;
    background: linear-gradient(135deg, #5e3dc5, #BD5E2D);
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const Content = styled.div`
    width: 100%;
    max-width: 315px;
    text-align: center;

    form{
        display: flex;
        flex-direction: column;
        margin-top: 30px;

        input{
            background: rgba(0, 0, 0, .15);
            border: 0;
            border-radius: 4px;
            height: 44px;
            padding: 0 15px;
            color: #FFF;
            margin: 0 0 10px;

            &::placeholder {
                color: rgba(255, 255, 255, 0.7);
            }
        }

        button {
            background: #41a219;
            border: 0;
            border-radius: 4px;
            color: #FFF;
            cursor: pointer;
            font-size: 16px;
            font-weight: bold;
            height: 44px;
            margin: 5px 0 0;
            transition: background .2s;

            &:hover{
                background: ${darken(0.03, '#41a219')};
            }
        }

        a{
            color: #FFF;
            font-size: 16px;
            margin-top: 25px;
            opacity: .8;

            &:hover{
                opacity: 1;
            }
        }

        span{
            align-self: flex-start;
            color: #ff8a8a;
            font-weight: bold;
            margin: 0 0 10px;
        }
    }

    img{
        max-width: 200px;
        width: 100%;
    }
`