import styled from 'styled-components';

export const Container = styled.div`
    margin: 50px auto;
    max-width: 1000px;
`

export const TextPostContent = styled.div`
    background-color: #FFF;
    border-radius: 5px 5px 0 0;
    display: block;
    padding: 10px 15px 20px;
    position: relative;
    text-align: left;
    
    > h2 {
        font-size: 28px;
        margin-bottom: 20px;

        .label-quest, .label-info{
            border-radius: 4px;
            color: #FFF;
            display: inline-block;
            font-size: 12px;
            font-weight: normal;
            margin-left: 20px;
            margin-top: 4px;
            padding: 3px 5px;
            vertical-align: text-top;
        }

        .label-quest{
            background-color: #6540bb;
        }

        .label-info{
            background-color: #ba5d32;
        }
    }

    > div {
        text-align: left;
    }

    > .sign{
        font-size: 11px;
        font-style: italic;
        margin-top: 20px;
        text-align: right;
    }

    > .options-bar{
        position: absolute;
        right: 15px;
        top: 10px;

        .icons {
            cursor: pointer;
            font-size: 20px;
            margin-left: 10px;
        }
    }
`