import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import Comments from '~/components/Comments';
import api from '~/services/api';
import parse from 'html-react-parser';
import { parseISO, format } from 'date-fns';
import { Link } from 'react-router-dom';
import { MdEdit, MdDelete } from 'react-icons/md';
import { deleteTextPostRequest } from '~/store/modules/textpost/actions';
import { Container, TextPostContent } from './styles';


export default function TextPostPage() {

    const [textPost, setTextPost] = useState();
    const { postId } = useParams();
    const dispatch = useDispatch();

    const userId = useSelector(state => state.user.profile).id;

    useEffect(() => {
        async function loadTextPost(){
            
            const response = await api.get(`textpost/${postId}`); 
            response.data.userIdLogged = userId;
            const data = response.data;

            setTextPost(data);
        }

        loadTextPost();
    }, [postId]);

    function HandleDelete(){
        dispatch(deleteTextPostRequest(textPost));
    }

    return (
        <Container>
            { textPost ? (
                <TextPostContent>
                    <h2>{textPost.title} <span className={textPost.text_type == 1 ? "label-quest" : "label-info"}>{textPost.text_type == 1 ? "Dúvida" : "Informativo"}</span></h2>
                    {
                        textPost.author.id == userId ?
                            <div className="options-bar"> <Link to={`/edittextpost/${postId}`} alt="Editar Postagem" title="Editar Postagem"><MdEdit className="icons" color="#BD5E2D" /></Link> <MdDelete onClick={HandleDelete} className="icons" color="#828283" /></div>
                        : ''
                    }
                    <div>
                        {parse(textPost.text_content)}
                    </div>
                    <div className="sign">
                        Em { format(parseISO(textPost.created_at), 'dd/MM/yyyy') } , por {textPost.author.name} 
                    </div>
                </TextPostContent>
                ) 
            : 
                (<TextPostContent>Nenhuma postagem encontrada</TextPostContent>) 
            }
            <Comments postId={postId} />
        </Container>
    );
}