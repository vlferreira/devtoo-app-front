import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin: 40px auto;
    max-width: 1100px;
    padding: 0 15px;

    .new-post-button {
        margin-bottom: 20px;
        text-align: right;

        > a {
            background-color: #41a219;
            border-radius: 4px;
            color: #FFF;
            padding: 5px;

            &:hover{
                background: ${darken(.04, '#41a219')};
            }
        }
    }
`;