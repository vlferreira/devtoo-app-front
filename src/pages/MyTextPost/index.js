import React from 'react';
import { useSelector } from 'react-redux';
import PostContentList from '~/components/PostContentList';
import { Container } from './styles';

export default function MyTextPost() {
    const userId = useSelector(state => state.user.profile).id;
    
    return (
        <Container>
            <div className="new-post-button">
                <a href="/newtextpost">+ Nova Postagem</a>
            </div>
            <PostContentList userId={userId} />
        </Container>
    );
}