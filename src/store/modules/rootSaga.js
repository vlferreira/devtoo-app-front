import { all } from 'redux-saga/effects';

import auth from './auth/sagas';
import user from './user/sagas';
import textpost from './textpost/sagas';
import comments from './comments/sagas';

export default function* rootSaga(){
    return yield all([auth, user, textpost, comments]);
}