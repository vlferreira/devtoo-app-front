export function insertTextPostRequest(data){
    return{
        type: '@textpost/INSERT_TEXTPOST_REQUEST',
        payload: { data }
    };
}
export function updateTextPostRequest(data){
    return{
        type: '@textpost/UPDATE_TEXTPOST_REQUEST',
        payload: { data }
    };
}

export function updateTextPostSuccess(textPost){
    return {
        type: '@textpost/UPDATE_TEXTPOST_SUCCESS',
        payload: { textPost }
    };
}

export function deleteTextPostRequest(data){
    return {
        type: '@textpost/DELETE_TEXTPOST_REQUEST',
        payload: { data }
    };
}

export function updateTextPostFailure(){
    return {
        type: '@textpost/UPDATE_TEXTPOST_FAILURE'
    };
}

export function deleteTextPostFailure(){
    return {
        type: '@textpost/DELETE_TEXTPOST_FAILURE'
    };
}