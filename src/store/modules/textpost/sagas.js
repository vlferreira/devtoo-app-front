import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import history from '~/services/history';
import api from '~/services/api';
import { deleteTextPostFailure, updateTextPostFailure, updateTextPostSuccess } from './actions';

export function* insertTextPost({ payload }){
    try{
        const { text_type, title, text_content, user_id } = payload.data;

        const textPost = Object.assign({ 
            text_type, 
            title, 
            text_content,
            user_id
        });
        const response = yield call(api.post, 'textpost', textPost);
        toast.success('Postagem realizada com sucesso!');

        history.push(`/textpost/content/${response.data.id}`);
    } catch (err) {
        toast.error('Erro ao enviar postagem, confira os dados digitados!');
        yield put(updateTextPostFailure());
    }
}

export function* updateTextPost({payload}){
    try{
        const {id, text_type, title, text_content } = payload.data;

        const textPost = Object.assign({ 
            id,
            text_type, 
            title, 
            text_content 
        });

        const response = yield call(api.put, 'textpost', textPost);
        toast.success('Postagem atualizada com sucesso!');
        
        yield put(updateTextPostSuccess(response.data));

        history.push(`/textpost/content/${response.data.id}`);

    } catch (err) {
        toast.error('Erro ao atualizar postagem, confira os dados digitados!');
        yield put(updateTextPostFailure());
    }
}

export function* deleteTextPost({payload}){
    try{
        const {id, user_id, is_valid, userIdLogged } = payload.data;

        const textPost = Object.assign({ 
            id,
            user_id,
            is_valid,
            userIdLogged
        });

        const response = yield call(api.put, 'textpost/delete', textPost);
        toast.success('Postagem removida com sucesso!');
        
        history.push('/mytextpost');

    } catch (err) {
        toast.error('Erro ao remover postagem!');
        yield put(deleteTextPostFailure());
    }
}

export default all([
    takeLatest('@textpost/INSERT_TEXTPOST_REQUEST', insertTextPost),
    takeLatest('@textpost/UPDATE_TEXTPOST_REQUEST', updateTextPost),
    takeLatest('@textpost/DELETE_TEXTPOST_REQUEST', deleteTextPost)
]);