import produce from 'immer';

const INITIAL_STATE = {
    textPost: null
};

export default function textPost(state = INITIAL_STATE, action){
    return produce(state, draft => {
        switch (action.type) {
            case '@textpost/INSERT_TEXTPOST_REQUEST':{
                draft.textPost = action.payload.textPost;
                break;
            }
            case '@textpost/UPDATE_TEXTPOST_REQUEST':{
                draft.textPost = action.payload.textPost;
                break;
            }
            case '@textpost/UPDATE_TEXTPOST_SUCCESS': {
                draft.textPost = action.payload.textPost;
                break;
            }
            case '@textpost/DELETE_TEXTPOST_REQUEST': {
                draft.textPost = action.payload.textPost;
                break;
            }
            default:
                return state;
        }
    });    
}