import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import history from '~/services/history';
import api from '~/services/api';
import { deleteCommentFailure, updateCommentFailure, updateCommentSuccess } from './actions';

export function* insertComment({ payload }){
    try{
        const { reference_post_id, text_content, user_id } = payload.data;

        const comment = Object.assign({ 
            reference_post_id, 
            text_content,
            user_id
        });

        const response = yield call(api.post, 'comments', comment);
        
        toast.success('Comentário postado com sucesso!');

    } catch (err) {
        toast.error('Erro ao enviar o comentário, confira os dados digitados!');
        yield put(updateCommentFailure());
    }
}

export function* updateComment({payload}){
    try{
        const { id, reference_post_id, text_content } = payload.data;

        const comment = Object.assign({ 
            id,
            reference_post_id, 
            text_content
        });

        const response = yield call(api.put, 'comments', comment);
        toast.success('Postagem atualizada com sucesso!');
        
        yield put(updateCommentSuccess(response.data));

    } catch (err) {
        toast.error('Erro ao enviar o comentário, confira os dados digitados!');
        yield put(updateCommentFailure());
    }
}

export function* deleteComment({payload}){
    try{
        const {id, reference_post_id, text_content } = payload.data;

        const comment = Object.assign({ 
            id,
            reference_post_id,
            text_content
        });

        const response = yield call(api.put, 'comment/delete', comment);
        toast.success('Comentário removido com sucesso!');

    } catch (err) {
        toast.error('Erro ao remover comentário!');
        yield put(deleteCommentFailure());
    }
}

export function* updateCommentVotes({payload}){
    try{
        const { id, votes } = payload.data;

        const comment = Object.assign({ 
            id,
            votes
        });

        const response = yield call(api.put, 'comments', comment);
        
        yield put(updateCommentSuccess(response.data));

    } catch (err) {
        toast.error('Erro ao realizar a votação do comentário');
        yield put(updateCommentFailure());
    }
}

export default all([
    takeLatest('@comment/INSERT_COMMENT_REQUEST', insertComment),
    takeLatest('@comment/UPDATE_COMMENT_REQUEST', updateComment),
    takeLatest('@comment/DELETE_COMMENT_REQUEST', deleteComment)
]);