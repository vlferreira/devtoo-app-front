import produce from 'immer';

const INITIAL_STATE = {
    comment: null
};

export default function comments(state = INITIAL_STATE, action){
    return produce(state, draft => {
        switch (action.type) {
            case '@comment/INSERT_COMMENT_REQUEST':{
                draft.comment = action.payload.comment;
                break;
            }
            case '@comment/UPDATE_COMMENT_REQUEST':{
                draft.comment = action.payload.comment;
                break;
            }
            case '@comment/UPDATE_COMMENT_SUCCESS': {
                draft.comment = action.payload.comment;
                break;
            }
            case '@comment/DELETE_COMMENT_REQUEST': {
                draft.comment = action.payload.comment;
                break;
            }
            default:
                return state;
        }
    });    
}