export function insertCommentRequest(data){
    return{
        type: '@comment/INSERT_COMMENT_REQUEST',
        payload: { data }
    };
}

export function updateCommentRequest(data){
    return{
        type: '@comment/UPDATE_COMMENT_REQUEST',
        payload: { data }
    };
}

export function updateCommentSuccess(comment){
    return {
        type: '@comment/UPDATE_COMMENT_SUCCESS',
        payload: { comment }
    };
}

export function deleteCommentRequest(comment){
    return {
        type: '@comment/DELETE_COMMENT_REQUEST',
        payload: { comment }
    };
}

export function updateCommentFailure(){
    return {
        type: '@comment/UPDATE_COMMENT_FAILURE'
    };
}

export function deleteCommentFailure(){
    return {
        type: '@comment/DELETE_COMMENT_FAILURE'
    };
}