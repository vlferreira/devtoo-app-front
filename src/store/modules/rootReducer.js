import { combineReducers } from 'redux';
import auth from './auth/reducer';
import user from './user/reducer';
import textpost from './textpost/reducer';
import comments from './comments/reducer';



export default combineReducers({
    auth,
    user,
    textpost,
    comments
});